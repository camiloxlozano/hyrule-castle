#!/bin/bash

source parts/colors.sh



function Bienvenida (){
printf "%s\n" "${blk}*********************************${end}"
printf "%s\n" "${cyn}   WELCOME TO DE HYRULE CASTLE   ${end}"
printf "%s\n" "${blk}*********************************${end}"

 echo ""
 echo "1)  >  New Game"
 echo "2)  >  Continuar"
 echo "3)  >  Exit"


	read option
}
function creacion_perfil (){


printf "%s" "${blk}Ingrese su nombre:  ${end} "
read nombre
clear
echo ""
echo "Eliga la nacion a la que quiere pertenecer:"
echo ""
printf "%s\n" "${cyn}1) --> Lufer (Mas vida) ${end} "
echo ""
printf "%s\n" "${red}2) --> Dark (Mas daño) ${end} "
echo ""
printf "%s\n" "${yel}3) --> Ecko (Mas poder de curacion) ${end} "
echo ""
printf "Nacion: "
read nacion

if [[ $nacion -eq 1 ]]
then
	nacion="Lufer"
fi

if [[ $nacion -eq 2 ]]
then
        nacion="Dark"
fi

if [[ $nacion -eq 3 ]]
then
        nacion="Ecko"
fi


echo "$nombre,$nacion" >> db/personaje.csv

clear

echo "${white}## Bienvenido al castillo de Hyrule, Has entrado para conquistar el castillo, tu objetivo final es matar al rey   ##"
echo "## En el camino de $nacion a Hyrule te encontraste 4 manzanas y algunas cosas que te seran utiles en para empezar ##"
echo "## pero no te confies... los enemigos de Hyrule, no tendran piedad. ##"
echo ""
echo  "${parpadea}Enter para empezar${end}${end}"
read
}


function plaza_principal(){
	#------->Camviar energia, rules 

infoSuperior="${cyn} $nombre -  $nacion ${end} | ${grn} Hp: 34${end} | ${org} Energia: 3 ${end} | ${mag} Ruless: 5 ${end}"

echo $infoSuperior
echo ""
printf "%s\n" "${blk}*********************************${end}"
printf "%s\n" "${yel}   PLAZA PRINCIPAL DEL CASTILLO  ${end}"
printf "%s\n" "${blk}*********************************${end}"

echo ""
echo "${gray}Usted se encuentra en la plaza principal"
echo ""
echo "¿Donde desea ir? ${end}"
echo ""
echo "|| 1-CASA  ||  2-TABERNA  || 3-HECHIZERO || 4-BOSQUE || 5-TORRE HYRULE ||"
echo ""
echo "${gray}Ctrl + c exit${end}"
read lugar
}

function casa(){
echo $infoSuperior
echo ""

printf "%s\n" "${blk}*********************************${end}"
printf "%s\n" "${blu}               CASA              ${end}"
printf "%s\n" "${blk}*********************************${end}"

echo ""
echo "${white}Tienes: 2 manzanas, 4 peras ${end}"
echo ""
echo "PRESIONE 1 PARA DORMIR  || PRESIONE 2 PARA COMER || PRESIONE 3 PARA SALIR"

read accionCasa

if [ $accionCasa -eq 3  ]
then
	clear


fi
}

function bosque (){
echo $infoSuperior
echo ""
printf "%s\n" "${blk}*********************************${end}"
printf "%s\n" "${grn}             BOSQUE              ${end}"
printf "%s\n" "${blk}*********************************${end}"


printf "%s\n" "${blk}Has entrado en el bosque del castillo de Hyrule${end}"


randomManzanas=$((RANDOM%5))

if [[ randomManzanas -gt 0 ]]
then
	echo "Haz encontrado $randomManzanas manzanas"
	else

	echo "Ninguna manzana"
fi

echo "Presiona ENTER para entrar ir a el castillo"
read

}


###LLamada de funciones###

ciclojuego=1

while [[ $ciclojuego -eq 1 ]]
do
	clear
	Bienvenida

	#New Profile
	if [[ $option -eq 1  ]]
	then
		clear
		creacion_perfil
		clear

		while [[ 1 -eq 1 ]]
		do
			clear

			#Plaza principal
			plaza_principal

			#Ha escogido entrar a la casa
			if [[ $lugar -eq 1 ]]
			then
				clear
				casa
			elif [[ $lugar -eq 4 ]]
			then
				clear
				bosque

			fi
		done

	else
		break
	fi

done
